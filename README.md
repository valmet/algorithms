# Algorithms

Collection of algorithms, inspired by [nryoung/algorithms](https://github.com/nryoung/algorithms)


Important note: this modules can be slow, because it's my own polygon for learning Elixir, but pull requests with fixes (or implemented algorithms) are welcome ;-)

# Already implemented

* Binary Search
* GCD & LCM
* Binary exponentiation
* Factorial
* Sleep sort
* Quick sort
